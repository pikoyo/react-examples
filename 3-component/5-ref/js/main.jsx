// 单行文本框输入
class Draw extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value: 0 };
    this.item = React.createRef();

    // 防止 this 指向问题
    this.draw = this.draw.bind(this);
  }

  draw() {
    var canvas = this.item.current;
    var context = canvas.getContext("2d");
    var sin = Math.sin(Math.PI / 6);
    var cos = Math.cos(Math.PI / 6);
    context.translate(100, 100);
    var c = 0;
    for (var i = 0; i <= 12; i++) {
      c = Math.floor((255 / 12) * i);
      context.fillStyle = "rgb(" + c + "," + c + "," + c + ")";
      context.fillRect(0, 0, 100, 10);
      context.transform(cos, sin, -sin, cos, 0, 0);
    }
  }

  render() {
    return (
      <div>
        <button onClick={this.draw}>绘图</button>
        <canvas height="200px" ref={this.item} />
      </div>
    );
  }
}

// 定义高阶组件
function logProps(WrappedComponent) {
  class LogProps extends React.Component {
    componentDidUpdate(prevProps) {
      console.log("old props:", prevProps);
      console.log("new props:", this.props);
    }

    render() {
      return <WrappedComponent {...this.props} />;
    }
  }

  return LogProps;
}

// 使用高阶组件
let setRef = (element) => {
  console.log(element);
};

const Link = function (props) {
  return <a {...props}>{props.children}</a>;
};

const LoggedLink = logProps("a"); // 组件对象为字符串时，表示的是 html 元素

function List() {
  let [list, setList] = React.useState([]);

  const addItem = (item) => {
    const newList = [...list, item];
    setList(newList);
  };

  const updateItem = (index, item) => {
    const newList = list.map((x, i) => (i == index ? item : x));
    setList(newList);
  };

  const deleteItem = (index) => {
    const newList = list.filter((x, i) => i != index);
    setList(newList);
  };
  return (
    <div>
      <ul>
        {list.map((item, i) => (
          <li key={i}>
            {item}
            <button onClick={() => updateItem(i, new Date().toString())}>
              更新
            </button>
            <button onClick={() => deleteItem(i)}>删除</button>
          </li>
        ))}
      </ul>

      <button onClick={() => addItem("项目")}>添加</button>
    </div>
  );
}

ReactDOM.render(
  <div>
    <Draw></Draw>
    <LoggedLink ref={setRef} href="https://www.baidu.com">
      链接
    </LoggedLink>
    <List></List>
  </div>,
  document.querySelector("#container")
);
