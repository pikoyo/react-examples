"use strict";

// 概述
ReactDOM.render(<p>Hello, world!</p>, document.querySelector("#container1"));

// 插入数据
const name = "kaz";
ReactDOM.render(<p>{name}</p>, document.querySelector("#container2"));

// 属性
ReactDOM.render(
  <span color="red">hello world!</span>,
  document.querySelector("#container3")
);

// 事件
function handleClick() {
  console.log("clicked");
}
ReactDOM.render(
  <button onClick={handleClick}>hello world!</button>,
  document.querySelector("#container4")
);

//闭合标签
ReactDOM.render(
  <img src="https://api.r10086.com/%E9%A3%8E%E6%99%AF%E7%B3%BB%E5%88%976.php" />,
  document.querySelector("#container5")
);

//包含子元素
ReactDOM.render(
  <div>
    <h1>标题</h1>
    <p>内容</p>
  </div>,
  document.querySelector("#container6")
);
