class ApiList extends React.Component {
  constructor(props) {
    super(props);
    this.state = { list: [] };
  }

  render() {
    let handleClick = async () => {
      console.log(this.state.list);
      this.setState({
        list: [],
      });
      console.log(this.state.list);
      let res = await fetch("https://api.github.com");
      let data = await res.json();
      console.log(this.state.list);
      this.setState({
        list: Object.entries(data),
      });
      console.log(this.state.list);
    };
    return (
      <div>
        <button onClick={handleClick}>加载数据</button>
        <ul>
          {this.state.list.map((x, i) => (
            <li key={i}>
              {x[0]}:{x[1]}
            </li>
          ))}
        </ul>
      </div>
    );
  }
}

ReactDOM.render(<ApiList></ApiList>, document.querySelector("#container"));
