const config = { theme: "light", setTheme: () => null };
const Config = React.createContext(config);
Config.displayName = "Config";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      theme: "dark",
    };
  }

  render() {
    let setTheme = () =>
      this.setState({ theme: this.state.theme === "dark" ? "light" : "dark" });
    return (
      <React.Fragment>
        <Config.Provider value={{ theme: this.state.theme, setTheme }}>
          <NavBar />
        </Config.Provider>
      </React.Fragment>
    );
  }
}

function NavBar() {
  return <ThemedButton />;
}

class ThemedButton extends React.Component {
  static contextType = Config;
  render() {
    return (
      <button {...this.props} onClick={() => this.context.setTheme()}>
        {this.context.theme}
      </button>
    );
  }
}

ReactDOM.render(<App></App>, document.querySelector("#container"));
