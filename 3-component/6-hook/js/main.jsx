function Counter() {
  // 定义一个状态
  let [count, setCount] = React.useState(0);
  let [updateCount, setUpdateCount] = React.useState(0);
  // 定义一个副作用钩子
  React.useEffect(() => {
    setUpdateCount(updateCount + 1);
  }, [count]);
  return (
    <div>
      <p>
        {count}:{updateCount}
      </p>
      <button onClick={() => setCount(count + 1)}>+1</button>
    </div>
  );
}

function ApiList() {
  // 定义一个状态
  let [list, setList] = React.useState([]);
  // 定义一个副作用钩子
  React.useEffect(async () => {
    await fetch("https://api.github.com")
      .then((res) => res.json())
      .then((data) => setList(Object.entries(data)));
  }, []);
  return (
    <ul>
      {list.map((x, i) => (
        <li key={i}>
          {x[0]}:{x[1]}
        </li>
      ))}
    </ul>
  );
}

ReactDOM.render(
  <React.Fragment>
    <Counter></Counter>
    <ApiList></ApiList>
  </React.Fragment>,
  document.querySelector("#container")
);
