class Counter extends React.Component {
  constructor(props) {
    super(props);
    this.state = { count: 0 };
  }

  render() {
    let handleClick = () => this.setState({ count: this.state.count + 1 });
    return (
      <div>
        <button onClick={handleClick}>+1</button>
        <p>{this.state.count}</p>
      </div>
    );
  }
}

// 使用插槽
function Panel(props) {
  return (
    <div>
      <h1>{props.title}</h1>
      <div>{props.children}</div>
    </div>
  );
}

const title = <font color="red">这是标题</font>;

const el = <Panel title={title}>这是内容</Panel>;

ReactDOM.render(
  <div>
    <Counter />
    {el}
  </div>,
  document.querySelector("#container")
);
