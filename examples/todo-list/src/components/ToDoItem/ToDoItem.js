import { List, Checkbox, Button } from "antd";

export function ToDoItem(props) {
  return (
    <List.Item>
      <Checkbox checked={props.data.done} onChange={() => props.onUpdate()}>
        {props.data.title}
      </Checkbox>
      <Button onClick={() => props.onDelete()} danger>
        删除
      </Button>
    </List.Item>
  );
}
