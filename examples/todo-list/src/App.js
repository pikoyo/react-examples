import { useState } from "react";
import { Layout, List, Input } from "antd";
import "./App.css";
import { ToDoItem } from "./components/ToDoItem";

const { Header, Content } = Layout;

function App() {
  let [list, setList] = useState([]);
  let [value, setValue] = useState([]);

  const addItem = (title) => {
    const newList = [...list, { title, done: false }];
    setList(newList);
  };

  const updateItem = (index) => {
    console.log(index);
    const newList = list.map((x, i) =>
      i !== index ? x : { ...x, done: !x.done }
    );
    setList(newList);
  };

  const deleteItem = (index) => {
    const newList = list.filter((x, i) => i !== index);
    setList(newList);
  };

  const handleKeyUp = (e) => {
    if (e.keyCode !== 13) return; // 13 是 Enter 键码
    addItem(e.target.value);
    setValue("");
  };

  const doneCount = list.filter((x) => x.done).length;

  return (
    <Layout className="layout">
      <Header>
        <div className="logo">TO-DO</div>
      </Header>
      <Content style={{ padding: "0 50px" }}>
        <div className="site-layout-content">
          <div>
            <Input
              onChange={(e) => setValue(e.target.value)}
              value={value}
              onKeyUp={handleKeyUp}
              placeholder="输入要做的事，按 Enter 添加"
            />
          </div>
          <div>
            <List
              dataSource={list}
              renderItem={(item, i) => (
                <ToDoItem
                  data={item}
                  onUpdate={() => updateItem(i)}
                  onDelete={() => deleteItem(i)}
                />
              )}
            ></List>
          </div>
          <div>
            已完成：{doneCount};总计：{list.length}
          </div>
        </div>
      </Content>
    </Layout>
  );
}

export default App;
