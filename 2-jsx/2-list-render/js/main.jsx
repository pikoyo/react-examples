const arr = [1, 2, 3, 4, 5];

const lists = arr.map((x) => <li>{x}</li>);

const el = <ul>{lists}</ul>;

ReactDOM.render(el, document.querySelector("#container"));
