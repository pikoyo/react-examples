let state = true;

function render() {
  let editState = () => {
    state = !state;
    render();
  };

  ReactDOM.render(
    <div>
      {state ? <p>state is true.</p> : <p>state is false.</p>}{" "}
      <button onClick={editState}>更改状态</button>{" "}
    </div>,
    document.querySelector("#container")
  );
}

render();
