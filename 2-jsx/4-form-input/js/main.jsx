let form = {
  readonly: "123",
};

let handleInputChange = (name) => (e) => {
  form[name] = e.target.value;
  render();
};

let handleCheckState = (name) => (e) => {
  form[name] = e.target.checked;
  render();
};

let textInput = (
  <input
    type="text"
    value={form["text"]}
    onChange={handleInputChange("text")}
  />
);

let textAreaInput = (
  <textarea value={form["textarea"]} onChange={handleInputChange("textarea")} />
);

let radioInput = (
  <input
    name="radio"
    type="radio"
    checked={form["radio"]}
    onChange={handleCheckState("radio")}
  />
);

let checkboxInput = (
  <input
    name="check"
    type="checkbox"
    checked={form["checkbox"]}
    onChange={handleCheckState("checkbox")}
  />
);

let selectInput = (
  <select value={form["select"]} onChange={handleInputChange("select")}>
    <option>星期一</option>
    <option>星期二</option>
    <option>星期三</option>
    <option>星期四</option>
    <option>星期五</option>
    <option>星期六</option>
    <option>星期日</option>
  </select>
);

let readOnlyInput = <input value={form["readonly"]} />;
let writableInput = <input value={form["writable"]} />;

function render() {
  ReactDOM.render(
    <div>
      {textInput}
      {textAreaInput}
      {radioInput}
      {checkboxInput}
      {selectInput}
      {readOnlyInput}
      {writableInput}
    </div>,
    document.querySelector("#container")
  );
}

render();
