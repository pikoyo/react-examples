import logo from './logo.svg';
import './App.css';
import mainCss from './main.module.css';

function App () {
  return (
    <div className="App">
      <header className="App-header">
              <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
          </header>
          <div className={mainCss.panel}>
              <div className={mainCss.title}>标题</div>
              <div>内容</div>
          </div>
          <div className={mainCss.post}>
              <div className={mainCss.title}>标题</div>
              <div>内容</div>
          </div>          
    </div>
  );
}

export default App;
