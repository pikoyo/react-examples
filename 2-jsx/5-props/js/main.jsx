const props = { color: "red", size: 18 };
const el = (
  <font color="blue" {...props}>
    hello world!
  </font>
);

ReactDOM.render(el, document.querySelector("#container"));
